package com.zuitt.batch193;

public class Main {
    public static void main(String[] args) {

//[USER] Instantiate new instance with parameterized parameter
    User firstUser = new User("Spongebob", "Squarepants", 7, "124 Conch Street, Bikini Bottom, Pacific Ocean");
        System.out.println("User's first name:");
        System.out.println(firstUser.getFirstName());
        System.out.println("User's last name:");
        System.out.println(firstUser.getLastName());
        System.out.println("User's age:");
        System.out.println(firstUser.getAge());
        System.out.println("User's address:");
        System.out.println(firstUser.getAddress());



//[COURSE] Instantiate new instance with empty constructor
         Course course1 = new Course();
        course1.setName("Java");
        course1.setDescription("Intro to Java");
        course1.setSeats(7);
        course1.setFee(1000);
        course1.setStartDate("June 27, 2022");
        course1.setEnDate("July 1, 2022");
        course1.setInstructor("Squidward");

        System.out.println("Course's name:");
        System.out.println(course1.getName());
        System.out.println("Course's description:");
        System.out.println(course1.getDescription());;
        System.out.println("Course's seats:");
        System.out.println(course1.getSeats());
        System.out.println("Course's Fee:");
        System.out.println(course1.getFee());
        System.out.println("Course's Start Date:");
        System.out.println(course1.getStartDate());
        System.out.println("Course's End Date:");
        System.out.println(course1.getEndDate());
        System.out.println("Course's Instructor's First Name;");
        System.out.println(course1.getInstructor());



    }
}


