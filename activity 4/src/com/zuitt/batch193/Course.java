package com.zuitt.batch193;

public class Course {

//Properties
    public String name;
    public String description;
    public int seats;
    public double fee;
    public String startDate;
    public String endDate;
    public String instructor;


    //Constructors
    //Default/Empty
    public Course(){};

    //Parameterized
    public Course(String name, String description, int seats, double fee, String startDate, String endDate, String instructor){
    this.name = name;
    this.description = description;
    this.seats = seats;
    this.fee = fee;
    this.startDate = startDate;
    this.endDate = endDate;
    this.instructor = instructor;
    }

//Getter for the Course
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
   }

   public String getDescription(){
        return this.description;
   }
    public void setDescription(String description){
        this.description = description;
    }

    public int getSeats(){
        return this.seats;
    }
    public void setSeats(int seats){
        this.seats= seats;
    }

    public double getFee(){
        return this.fee;
    }
    public void setFee(double fee){
        this.fee = fee;
    }

    public String getStartDate(){
        return this.startDate;
    }
    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public String getEndDate(){
        return this.endDate;
    }
    public void setEnDate(String endDate){
        this.endDate= endDate;
    }

    public String getInstructor(){
        return this.instructor;
    }
    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

}






