package com.zuitt.batch193;

public class User {

//Properties

    public String firstName;
    public String lastName;
    public int age;
    public String address;

    //Constructor
    //Default/Empty
    public User(){};

    //Parameterized
    public User(String firstName, String lastName, int age, String address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
    }
//Getter for User
       public String getFirstName(){
           return this.firstName;
        }
        public String getLastName(){
           return this.lastName;
        }
        public int getAge(){
            return this.age;
        }
        public String getAddress(){
            return this.address;
        }

}
